﻿using UnityEngine;
using System.Collections;

public class ChibiStand : MonoBehaviour {
	
	
	public float gravity = -9.81f;
	public float newScale;
	public Rigidbody ridge;
	// Use this for initialization
	void Start () {
		Physics.gravity = Vector3.zero;
		ridge = GetComponent<Rigidbody> ();
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		if (ScoreHealth.health <= 0) {
			Destroy (gameObject, 0);
		}

		Vector3 direction = transform.position; //- planetaryCenter;
		Vector3 force = direction.normalized * gravity;
		
		// accelerate the object in that direction
		ridge.AddForce (force, ForceMode.Acceleration);
		//ridge.velocity= new Vector3(Time.deltaTime*20,ridge.velocity.y, Time.deltaTime*20);
	}
	void ChibiDeath ()
	{
		//play death sound
		//newScale = Mathf.Lerp(1f, .2f, Time.deltaTime);
		//transform.localScale = new Vector3(newScale, newScale, newScale);
		ScoreHealth.health--; 
		ChibiMaker.chibiCount--;
		Destroy (gameObject, .3f);
		
	}

	
	
}
