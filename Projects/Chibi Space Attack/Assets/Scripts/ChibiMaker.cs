﻿using UnityEngine;
using System.Collections;

public class ChibiMaker : MonoBehaviour {

	public float timer;
	public static float nextChibi;
	public int whichChibi;
	public int rotaterx;
	public int rotatery;
	public int rotaterz;
	public static int chibiCount;
	public GameObject planet;
	public GameObject holder;
	public GameObject chibi1;
	public GameObject chibi2;
	public GameObject chibi3;
	public GameObject chibi4;
	public GameObject chibi5;
	public static int flagger;
	public int identifier;
	public int changer;


	public float counter;
	// Use this for initialization
	void Start () {
		timer = 17f;
		nextChibi = 22f; 
		flagger = 1;
		chibiCount = 0;
		counter = 0;
	}
	
	// Update is called once per frame
	void Update () { 

		counter += Time.deltaTime;
		if (counter > 9 && ScoreHealth.health > 0) {
			if (chibiCount < 22) {
				if (identifier == flagger) {
					nextChibi += Time.deltaTime;
				}
				if (nextChibi > timer) {

					if (identifier == flagger) {

						whichChibi = Random.Range (1, 6);

						if (whichChibi == 1) {
							holder = (GameObject)Instantiate (chibi1, transform.position, Quaternion.identity);
							holder.transform.eulerAngles = new Vector3 (
							holder.transform.eulerAngles.x - rotaterx,
							holder.transform.eulerAngles.y - rotatery,
							holder.transform.eulerAngles.z - rotaterz
							);       
						}

						if (whichChibi == 2) {
							holder = (GameObject)Instantiate (chibi2, transform.position, Quaternion.identity);
							holder.transform.eulerAngles = new Vector3 (
							holder.transform.eulerAngles.x - rotaterx,
							holder.transform.eulerAngles.y - rotatery,
							holder.transform.eulerAngles.z - rotaterz
							);    
						}

						if (whichChibi == 3) {
							holder = (GameObject)Instantiate (chibi3, transform.position, Quaternion.identity);
							holder.transform.eulerAngles = new Vector3 (
							holder.transform.eulerAngles.x - rotaterx,
							holder.transform.eulerAngles.y - rotatery,
							holder.transform.eulerAngles.z - rotaterz
							);    
						}

						if (whichChibi == 4) {
							holder = (GameObject)Instantiate (chibi4, transform.position, Quaternion.identity);
							holder.transform.eulerAngles = new Vector3 (
							holder.transform.eulerAngles.x - rotaterx,
							holder.transform.eulerAngles.y - rotatery,
							holder.transform.eulerAngles.z - rotaterz
							);    
						}

						if (whichChibi == 5) {
							holder = (GameObject)Instantiate (chibi5, transform.position, Quaternion.identity);
							holder.transform.eulerAngles = new Vector3 (
							holder.transform.eulerAngles.x - rotaterx,
							holder.transform.eulerAngles.y - rotatery,
							holder.transform.eulerAngles.z - rotaterz
							);    
						}
				

						holder.transform.parent = planet.transform;
						chibiCount++;
						nextChibi = 0;
						flagger = changer;
					}
			
				}
			}
		}
	}
}
