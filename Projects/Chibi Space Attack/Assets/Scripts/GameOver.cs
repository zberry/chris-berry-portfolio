﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class GameOver : MonoBehaviour {


 
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if (ScoreHealth.health >= 1)
        {
            transform.GetChild(0).gameObject.SetActive(false);
        }

        if (ScoreHealth.health <= 0) {
			transform.GetChild(0).gameObject.SetActive(true);
		}
	
	}
}
