﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreHealth : MonoBehaviour {

	public static int score =0;
	public static int health = 5;
	public float timer;
	public Text scoreT; 
	public Image healthT;
	public int chibiNum;
	public Sprite text1;
	public Sprite text2;
	public Sprite text3; 
	public Sprite text4;
	public Sprite text5; 
	public Sprite text6;
	// Use this for initialization
	void Start () {
		score = 0;
		health = 5;  
		timer = 0;
		scoreT.text = "Score: " + score;
		healthT.sprite = text1;
		chibiNum = ChibiMaker.chibiCount;
	
	} 
	
	// Update is called once per frame
	void Update () {

		scoreT.text = "Score: " + score;
		timer += Time.deltaTime;

		if (chibiNum > ChibiMaker.chibiCount) {
			timer = 0;
		} else
			chibiNum = ChibiMaker.chibiCount;

		if(timer >= 30)
		{
			if(health < 5)
			{
				health++;
			}

			timer=0;
		}
		if(health == 5 )
		{
			healthT.sprite = text1;
		}
		if(health == 4 )
		{
			healthT.sprite = text2;
		}
		if(health == 3 )
		{
			healthT.sprite = text3;
		}
		if(health == 2 )
		{
			healthT.sprite = text4;
		}
		if(health == 1 )
		{
			healthT.sprite = text5;
		}

		if(health <= 0)
		{
			healthT.sprite = text6;
            
                  
   		 }
        if (Input.GetKeyDown("space"))
        {
            health = 0;
        }

    }
}
