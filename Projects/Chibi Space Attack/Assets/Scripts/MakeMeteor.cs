﻿using UnityEngine;
using System.Collections;

public class MakeMeteor : MonoBehaviour {

	public GameObject meteor;
	public int meteorCount;
	public float timeBetween;
	public float timeCounter;
	public static float meteorSpeed;
	public float xCor;
	public float yCor;
	public float zCor;
	public int whichOne;
	public static float scale;
	public int pacer;
	public int scaler;
	public CameraOpen cam;
	public float counter;
     
    void Start() {
		timeBetween = 6f;
		meteorCount = 0;
		timeCounter = 5f;
		meteorSpeed = .08f;
		whichOne = 0;
		pacer = 0;
		scaler = 3;


    }
	//change scale factor


	// Update is called once per frame
	void Update () {

		counter += Time.deltaTime;
		if (counter > 9 && ScoreHealth.health > 0) {

			timeCounter += Time.deltaTime;
			if (timeCounter > timeBetween) {

				yCor = Random.Range (-24f, 24f);
				whichOne = Random.Range (1, 5);

		
				if (whichOne == 1) {
					xCor = Random.Range (-24f, 24f);
					zCor = -24f;
				}
				if (whichOne == 2) {
					xCor = Random.Range (-24f, 24f);
					zCor = 24f;
				}
				if (whichOne == 3) {
					zCor = Random.Range (-24f, 24f);
					xCor = 24f;
				
				}
				if (whichOne == 4) {
					zCor = Random.Range (-24f, 24f);
					xCor = -24f;
				}

				scale = Random.Range (0f, .3f);

				Instantiate (meteor, new Vector3 (xCor, yCor, zCor), Quaternion.identity);
				timeCounter = 0;
				meteorCount++;

				if (meteorCount == 10) {
					meteorCount = 0;


					if (pacer < scaler) {
						if (timeBetween > 3) {
							timeBetween -= .4f;

						}
						pacer++;
					} else {
						if (pacer >= scaler) {
							timeBetween += .5f;
							pacer = 0;


						}
					}


				}
			}

		}
	
	}
}
