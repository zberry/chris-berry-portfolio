﻿using UnityEngine;
using System.Collections;

public class ButtonNextLevel : MonoBehaviour
{
    public void NextLevelButton(int index)
    {
		ScoreHealth.health = 5;
		ScoreHealth.score = 0;
		ChibiMaker.nextChibi = 22f;
		ChibiMaker.chibiCount = 0;
		ChibiMaker.flagger = 1;
        Application.LoadLevel(index);
    }

}