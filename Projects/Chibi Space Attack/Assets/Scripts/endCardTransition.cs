﻿using UnityEngine;
using System.Collections;

public class endCardTransition : MonoBehaviour {

	void Update () {
        if (ScoreHealth.health == 0)
        {
            GetComponent<Animation>().Play();
            ScoreHealth.health -= 1;
        }
    }
}
