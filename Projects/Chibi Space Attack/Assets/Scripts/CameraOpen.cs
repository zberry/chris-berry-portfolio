﻿using UnityEngine;
using System.Collections;

public class CameraOpen : MonoBehaviour {

	public bool begin;
	public bool middle;
	public bool end;
	public float speed;
	public float counter;
	public AudioClip clip;
	public bool start;

	// Use this for initialization
	void Start () {
	
		begin = true;
		middle = true;
		end = true;
		start = false;
	}
	
	// Update is called once per frame
	void Update () {
	
		if (begin) {
			transform.position = Vector3.MoveTowards (transform.position, new Vector3 (0, 2.8158f, 2.0137f), speed);
		}
	

		counter += Time.deltaTime;

		if (gameObject.transform.position == new Vector3 (0, 2.8158f, 2.0137f) && middle) {
			AudioSource.PlayClipAtPoint(clip, Camera.main.transform.position);
			begin=false;
			middle = false;
		}

		if (counter > 7 && end) {
			transform.position = Vector3.MoveTowards (transform.position, new Vector3 (0, 0, 10.751f), speed);
		}

		if (gameObject.transform.position ==  new Vector3 (0, 0, 10.751f) && middle) {

			end=false;
			start=true;
		}

	
	
	}
}
