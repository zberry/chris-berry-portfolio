﻿var targetItem : GameObject;
      
var rotationRate : float = 1.0;
private var wasRotating;
     
private var scrollPosition : Vector2 = Vector2.zero;
private var scrollVelocity : float = 0;
private var timeTouchPhaseEnded: float;
private var inertiaDuration : float = 0.5f;
     
private var itemInertiaDuration : float = 1.0f;
private var itemTimeTouchPhaseEnded: float;
private var rotateVelocityX : float = 0;
private var rotateVelocityY : float = 0;
public var counter: float = 0;     
     
var hit: RaycastHit;
     
private var layerMask = (1 <<  8) | (1 << 2);

     
     
function Start()
{
    layerMask =~ layerMask;  
    counter=0;  
}
     
function FixedUpdate()
{
	counter+= Time.deltaTime;
	
	if(counter > 9)
	{
         
    if (Input.touchCount > 0) 
    { 
        var theTouch : Touch = Input.GetTouch(0);       
                 
        var ray = Camera.main.ScreenPointToRay(theTouch.position);
       
          
                     
        if(!Physics.Raycast(ray,hit,50,layerMask))
        {    
            
            if(Input.touchCount == 1)
            {
                                 
                if (theTouch.phase == TouchPhase.Began) 
                {
                    wasRotating = false;    
                }        
                                  
                if (theTouch.phase == TouchPhase.Moved) 
                {
                                       
                    targetItem.transform.Rotate(-theTouch.deltaPosition.y* rotationRate, -theTouch.deltaPosition.x * rotationRate,0,Space.Self);
                    wasRotating = true;
                }   
                if (theTouch.phase == TouchPhase.Ended || theTouch.phase == TouchPhase.Canceled)
                {
                    if (wasRotating==true)
                    {
                        if(Mathf.Abs(theTouch.deltaPosition.x) >=5)
                            rotateVelocityX = theTouch.deltaPosition.x / theTouch.deltaTime;
                    }
                    if(Mathf.Abs(theTouch.deltaPosition.y) >=5)
                    {
                        rotateVelocityY = theTouch.deltaPosition.y / theTouch.deltaTime;
                    }
                    itemTimeTouchPhaseEnded = Time.time;
                }                 
            }                                        
        }
    }
    }
}