﻿using UnityEngine;
using System.Collections;

public class MeteorMovement : MonoBehaviour {
	public float speed;
	public explode blowUp;
	public Renderer rend;
	public bool isColiderEnabled;
    public AudioClip astBoom;
    private AudioSource source;
    // Use this for initialization
    void Start () {
		speed = Random.Range (.01f, MakeMeteor.meteorSpeed);
		transform.localScale += new Vector3( MakeMeteor.scale, MakeMeteor.scale, MakeMeteor.scale);
		rend = GetComponent<Renderer>();
		isColiderEnabled = true;
        source = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
		transform.position = Vector3.MoveTowards(transform.position, new Vector3(0,0,0), speed);
		transform.Rotate (0, 0, 3);
	
	}

	void OnCollisionEnter (Collision colide)
	{
		if (isColiderEnabled) {
			blowUp = gameObject.GetComponentInChildren<explode> ();
			blowUp.flag = true;
			rend.enabled = false;
			isColiderEnabled = false;
			colide.transform.SendMessage ("ChibiDeath", SendMessageOptions.DontRequireReceiver);
            if(ScoreHealth.health > 0)
			    ScoreHealth.score+= 10 * ChibiMaker.chibiCount;

			Destroy (gameObject, 2f);
            source.PlayOneShot(astBoom, 1.0f);
		}
	}
} 
