﻿using UnityEngine;
using System.Collections;

public class person : MonoBehaviour {

	public PlanetGravitation grav;
	public float gravity = -9.81f;
	public  bool landed= false;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void FixedUpdate () {

		if (landed) {

			Vector3 direction = grav.transform.position; //- planetaryCenter;
			Vector3 force = direction.normalized * gravity;

			// accelerate the object in that direction
			grav.GetComponent<Rigidbody> ().AddForce (force, ForceMode.Acceleration);
		}
	
	}
}
