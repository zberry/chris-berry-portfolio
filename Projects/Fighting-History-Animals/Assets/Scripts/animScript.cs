using UnityEngine;
using System.Collections;

public class animScript : MonoBehaviour {

  private float distanceToGround;
  public int joystickNumber;

	void Update () {

    if (joystickNumber == 1) {
      distanceToGround = main.m.distanceToGroundp1;
    }
    else {
      distanceToGround = main.m.distanceToGroundp2;
    }

		float isAir = distanceToGround;
		bool Punch = Input.GetButtonDown("punch"+joystickNumber);
		bool Kick = Input.GetButtonDown("kick"+joystickNumber);
		float vert = Input.GetAxis("VerticalP"+joystickNumber);
		float horz = Input.GetAxis("HorizontalP"+joystickNumber);
		
		GetComponent<Animator>().SetFloat("isAir", isAir);
		GetComponent<Animator>().SetBool("Punch", Punch);
		GetComponent<Animator>().SetBool("Kick", Kick);
		GetComponent<Animator>().SetFloat("Crouch", vert);
		GetComponent<Animator>().SetFloat("Horz", horz);
	}
}