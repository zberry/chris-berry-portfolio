﻿using UnityEngine;
using System.Collections;

public class LevelLoader : MonoBehaviour {

  public void LoadLevel(string lvl) {
    Application.LoadLevel(lvl);
  }

  public void LoadMenu() {
  	Application.LoadLevel("menu");
  }
  public void LoadFight() {
  	Application.LoadLevel("play");
  }
}
