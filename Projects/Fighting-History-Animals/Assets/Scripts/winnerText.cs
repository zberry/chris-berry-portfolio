﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class winnerText : MonoBehaviour {

	public Text winTxt;
	public GameObject winPanel;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if (main.m.healthp1 <= 0) {
			winTxt.GetComponent<Text>().text = "Player 2 wins";
			winPanel.GetComponent<CanvasGroup>().alpha = 1;
			winPanel.GetComponent<CanvasGroup>().interactable = true;
		}
		if (main.m.healthp2 <= 0) {
			winTxt.GetComponent<Text>().text = "Player 1 wins";
			winPanel.GetComponent<CanvasGroup>().alpha = 1;
			winPanel.GetComponent<CanvasGroup>().interactable = true;
		}
		if (Timer.timer == 0) {
			winTxt.GetComponent<Text>().text = (main.m.healthp1 > main.m.healthp2) ? "Player 1 wins":"Player 2 wins";
		}
	}
}
