﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CharacterSpawn : MonoBehaviour {

  public GameObject leftCat, leftSkunk, rightCat, rightSkunk, hud;
  public Sprite hud1, hud2;

	void Start () {

    leftCat.SetActive(true);
    rightSkunk.SetActive(true);

    if (CharacterSelection.characterSelect == 1)
    {
      leftCat.transform.position = new Vector3(-6.5f, transform.position.y, transform.position.z);
      rightSkunk.transform.position = new Vector3(6.5f, transform.position.y, transform.position.z);
      leftCat.GetComponent<MovementControls>().joystickNumber = 1;
      rightSkunk.GetComponent<MovementControls>().joystickNumber = 2;
      hud.GetComponent<Image>().sprite = hud1;
    }
    if (CharacterSelection.characterSelect == 2)
    {
      rightSkunk.transform.position = new Vector3(-6.5f, transform.position.y, transform.position.z);
      leftCat.transform.position = new Vector3(6.5f, transform.position.y, transform.position.z);
      leftCat.GetComponent<MovementControls>().joystickNumber = 2;
      rightSkunk.GetComponent<MovementControls>().joystickNumber = 1;
      hud.GetComponent<Image>().sprite = hud2;
    }
	}
}
