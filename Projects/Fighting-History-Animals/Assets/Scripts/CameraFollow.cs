﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

	public GameObject Player1;
	public GameObject Player2;
	public GameObject container;
	public Camera cam;
	private float zoom = 5f;

	void Update () {
		SetTransformX(((Player1.transform.position.x + Player2.transform.position.x) / 2));
		SetTransformY(((Player1.transform.position.y + Player2.transform.position.y) / 2));
		SetViewSize(zoom);
	}

	void SetTransformX(float n) {
  	container.transform.position = new Vector3(n, transform.position.y, transform.position.z);
	}
	void SetTransformY(float n) {
  	container.transform.position = new Vector3(transform.position.x, n, transform.position.z);
	}
	void SetViewSize(float n) {
		cam.orthographicSize = n;
	}
}