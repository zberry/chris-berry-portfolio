﻿using UnityEngine;
using System.Collections;

public class MovementControls : MonoBehaviour {

  private Vector3 movementVector;
  private CharacterController chaController;
  public int movementSpeed = 10;
  public int jumpPower = 15;
  public int joystickNumber;
  public int gravity = 40;

  private float hitpoints;
  private float health = 100f;

  private float distanceToGround;

  public GameObject opponent;
  private string opponentStr;

  public Transform target;

  public float currentHealthp1 = 200;
  public float currentHealthp2 = 200;

  private bool hitAnim;
  public bool isTouch;

  public AudioClip PunchS;
  public AudioClip KickS;
  public AudioClip BlockS;

  private AudioSource source;

	void Start () {
        chaController = GetComponent<CharacterController>();
        opponentStr = opponent.ToString();
        Debug.Log(opponent + "  " + opponentStr);
        hitAnim = false;
        isTouch = false;
        source = GetComponent <AudioSource> ();
        DestroyMusic.gameRun = 1;
	}
	
	void Update () {
        if (joystickNumber == 1) {
            distanceToGround = main.m.distanceToGroundp1;
            health = main.m.healthp2;
            main.m.healthp2 = health;
        }
        else {
            distanceToGround = main.m.distanceToGroundp2;
            health = main.m.healthp1;
            main.m.healthp1 = health;
        }

        SwapPlayer();
        SetTransform();

        RaycastHit hit;

        if (Physics.Raycast(transform.position, -Vector3.up, out hit)) {
            if (joystickNumber == 1) {
                main.m.distanceToGroundp1 = hit.distance;
            }
            else {
                main.m.distanceToGroundp2 = hit.distance;
            }
        }

        string joyStick = joystickNumber.ToString();
        
        if (this.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CrouchBlock")) {
            movementVector.x = 0;
        }
        else if (this.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Block")) {
            movementVector.x = Input.GetAxis("HorizontalP" + joyStick) * (movementSpeed/2);
        }
        else {
            movementVector.x = Input.GetAxis("HorizontalP" + joyStick) * movementSpeed;
        }
        
        movementVector.y -= gravity * Time.deltaTime;
        chaController.Move(movementVector * Time.deltaTime);

        if (distanceToGround <= 0.1f)
        {
            movementVector.y = 0;
        }

        if (Input.GetAxis("VerticalP"+joyStick) > 0 && distanceToGround <= 0.1f)
        {
            movementVector.y = jumpPower;
        }
        if (Input.GetAxis("VerticalP"+joyStick) < 0 && Input.GetAxis("HorizontalP"+joyStick) < 0) {
            movementVector.x = 0;
        }

        Anim();

        if (Input.GetKeyDown("z") && isTouch == true && !main.m.blockp2){
            source.PlayOneShot(PunchS, 1);
        }
        if (Input.GetKeyDown("z") && isTouch == true && main.m.blockp2){
            source.PlayOneShot(BlockS, 1);
        }
        if (Input.GetKeyDown("c") && isTouch == true && !main.m.blockp2){
            source.PlayOneShot(KickS, 1);
        }
        if (Input.GetKeyDown("c") && isTouch == true && main.m.blockp2){
            source.PlayOneShot(BlockS, 1);
        }
        if (Input.GetKeyDown("g") && isTouch == true && !main.m.blockp1){
            source.PlayOneShot(PunchS, 1);
        }
        if (Input.GetKeyDown("g") && isTouch == true && main.m.blockp1){
            source.PlayOneShot(BlockS, 1);
        }
        if (Input.GetKeyDown("f") && isTouch == true && !main.m.blockp1){
            source.PlayOneShot(KickS, 1);
        }
        if (Input.GetKeyDown("f") && isTouch == true && main.m.blockp1){
            source.PlayOneShot(BlockS, 1);
        }
	}

    void Anim() {
        float isAir = distanceToGround;
        bool Punch = Input.GetButtonDown("punch"+joystickNumber);
        bool Kick = Input.GetButtonDown("kick"+joystickNumber);
        float vert = Input.GetAxis("VerticalP"+joystickNumber);
        float horz = Input.GetAxis("HorizontalP"+joystickNumber);

        GetComponent<Animator>().SetFloat("isAir", isAir);
        GetComponent<Animator>().SetBool("Punch", Punch);
        GetComponent<Animator>().SetBool("Kick", Kick);
        GetComponent<Animator>().SetFloat("Crouch", vert);
        
        if (transform.position.x >= opponent.transform.position.x) {
            GetComponent<Animator>().SetFloat("Horz", -horz);
        }
        else {
            GetComponent<Animator>().SetFloat("Horz", horz);
        }
    }

    void OnTriggerStay(Collider other) {
        if (joystickNumber == 1) {
            if (other.gameObject.tag == "Player2") {
                Hit(hitpoints);
                main.m.healthp2 = health;
                currentHealthp2 = main.m.healthp2;
                isTouch = true;
            }
            else{
                currentHealthp2 = 200;
                isTouch = false;
            }

        }
        else {
            if (other.gameObject.tag == "Player1") {
                Hit(hitpoints);
                main.m.healthp1 = health;
                currentHealthp1 = main.m.healthp1;
                isTouch = true;
            }
            else{
                currentHealthp1 = 200;
                isTouch = false;
            }

        }
    }

    void Hit(float a) {
      if (joystickNumber == 1) {
        if (main.m.blockp2) {
          hitpoints = a/2;
        }
        else {
          hitpoints = a;
        }
      }
      else {
        if (main.m.blockp1) {
          hitpoints = a/2;
        }
        else {
          hitpoints = a;
        }            
      }
			if (joystickNumber == 2) {
				if (hitpoints >= 1 && !main.m.blockp1 && main.m.healthp1 == currentHealthp1){
					main.hitCountp2 += 1;
					main.hitTimerp2 = 5;
					hitAnim = true;
					opponent.GetComponent<Animator>().SetTrigger("Hit");
         	Debug.Log("hitanim" + hitAnim);
         }
      } else {
        if (hitpoints >= 1 && !main.m.blockp2 && main.m.healthp2 == currentHealthp2){
						main.hitCountp1 += 1;
						main.hitTimerp1 = 5;
						hitAnim = true;
						opponent.GetComponent<Animator>().SetTrigger("Hit");
           	Debug.Log("hitanim" + hitAnim);
           }
        }
      health -= hitpoints;
      hitpoints = 0f;
      // Debug.Log("hit");
      // StartCoroutine(smallPause(0.1f));
    }


    void SetTransform() {
        transform.position = new Vector3(transform.position.x, transform.position.y, -0.5f);
    }

    void SwapPlayer() {
        if (transform.position.x >= opponent.transform.position.x) {
            transform.localScale = new Vector3(1, 1, -1);
            transform.eulerAngles = new Vector3(0, 0, 0);
            if (joystickNumber == 1) {
                if (movementVector.x > 0f) {
                    main.m.blockp1 = true;
                }
                else {
                    main.m.blockp1 = false;
                }
            }
            else {
                if (movementVector.x > 0f) {
                    main.m.blockp2 = true;
                }
                else {
                    main.m.blockp2 = false;
                }
            }
        }
        else {
            transform.localScale = new Vector3(1, 1, 1);
            transform.eulerAngles = new Vector3(0, 180, 0);
            if (joystickNumber == 1) {
                if (movementVector.x < 0f) {
                    main.m.blockp1 = true;
                }
                else {
                    main.m.blockp1 = false;
                }
            }
            else {
                if (movementVector.x < 0f) {
                    main.m.blockp2 = true;
                }
                else {
                    main.m.blockp2 = false;
                }
            }

        }
    }

    private IEnumerator smallPause(float p) {
        Time.timeScale = 0.000001f;
        yield return new WaitForSeconds(p * Time.timeScale);
        Time.timeScale = 1.0f;
    }

}