using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class main : MonoBehaviour {
	public static main m;

	public float healthp1 = 100;
	public float healthp2 = 100;

	public bool blockp1 = false;
	public bool blockp2 = false;
	// <summary>
	// combo stuff! ~~~~~~~~~~~~~
	// </summary>
	public int combop1 = 0;
	public int combop2 = 0;

	public static int hitCountp1 = 0;
	public static int hitCountp2 = 0;

	public static float hitTimerp1 = 0;
	public static float hitTimerp2 = 0;
	// <summary>
	// end combo stuff! ~~~~~~~~~~~~~
	// </summary>
	public float distanceToGroundp1 = 0;
	public float distanceToGroundp2 = 0;

	public static float hpoint1;
	public static float hpoint2;

	private float barHealth1;
	private float barHealth2;
	public Image bar1, bar2;
	public GameObject combopanelp1;
	public GameObject combopanelp2;

	// Use this for initialization
	void Start () {
		m = this;
	}
	
	// Update is called once per frame
	void Update () {
		hpoint1 = healthp1;
		hpoint2 = healthp2;

		combop1 = hitCountp1;
		combop2 = hitCountp2;


    bar1.GetComponent<Image>();
    bar1.fillAmount = barHealth1; 
    barHealth1 = healthp1 / 100;
   
		 bar2.GetComponent<Image>();
    bar2.fillAmount = barHealth2;
    barHealth2 = healthp2 / 100;

    if (healthp1 <= 65)
		bar1.color = Color.yellow;
		if (healthp1 <= 32)
		bar1.color = Color.red;

		if (healthp2 <= 65)
		bar2.color = Color.yellow;
		if (healthp2 <= 32)
		bar2.color = Color.red;

		hitTimerp1 -= Time.deltaTime;
		hitTimerp2 -= Time.deltaTime;

		if (hitTimerp1 >= 0.1){
			combopanelp1.SetActive(true);
		}
		else {
			combopanelp1.SetActive(false);
		}

		if (hitTimerp2 >= 0.1){
			combopanelp2.SetActive(true);
		}
		else{
			combopanelp2.SetActive(false);
		}

		if(hitTimerp1 <= 0){
			hitCountp1 = 0;
		}	
		if(hitTimerp2 <= 0){
			hitCountp2 = 0;
		}	


		//if (healthp1 <= 0)
		//Application.LoadLevel("P2Win");
		//if (healthp2 <= 0)
		//Application.LoadLevel("P1Win");

		if (Input.GetButtonDown("Quit")) {
			Debug.Log("quit");
			Arcade.Quit();
		}
		
		}
	}

