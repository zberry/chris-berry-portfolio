﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class comboTextP2 : MonoBehaviour {

	public Text comboTxt2;

	// Use this for initialization
	void Start () {
		comboTxt2 = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
		comboTxt2.text = "" + main.hitCountp2;
	}
}
