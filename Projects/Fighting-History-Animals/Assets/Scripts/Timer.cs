﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Timer : MonoBehaviour {

	public static float timer = 99.0f;
	string displayNumber;
	public Text timeText; 
	
	void Start () {
	
	}

	void Update () {
	
		timer -= Time.deltaTime;

		if (timer <= 0f) {
			timer = 0f;
		}

		displayNumber = timer.ToString ("0");

		timeText.text = "" + displayNumber;
	}
}
