﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class comboTextP1 : MonoBehaviour {

	public Text comboTxt1;

	// Use this for initialization
	void Start () {
		comboTxt1 = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
		comboTxt1.text = "" + main.hitCountp1;
	}
}
