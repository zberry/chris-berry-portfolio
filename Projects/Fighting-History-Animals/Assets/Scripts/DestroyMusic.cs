﻿using UnityEngine;
using System.Collections;

public class DestroyMusic : MonoBehaviour {

	public static int gameRun = 0;

	public static DestroyMusic instance = null;

	// Use this for initialization
	void Start () {
		if (instance == null)
			instance = this;
		else if (instance != this)
			Destroy(gameObject);
		DontDestroyOnLoad(gameObject);

	
	}
	
	// Update is called once per frame
	void Update () {
		DontDestroyOnLoad(gameObject);

		if (gameRun == 1){
			Destroy (gameObject);
		}
	}
}
