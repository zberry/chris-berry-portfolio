﻿using UnityEngine;
using System.Collections;

public class charSelect : MonoBehaviour {

	private bool p1Selected = false;
	private bool p2Selected = false;

	private int level = 0;

	public int p1Char;
	public int p2Char;
	
	void Update () {

		if (p2Selected == true) {
			Application.LoadLevel(level);
		}
	
	}

	void CharacterSelect(int n) {
		if (p1Selected == false) {
			p1Char = n;
			p1Selected = true;
		}
		else {
			p2Char = n;
			p2Selected = true;
		}
	}
}
