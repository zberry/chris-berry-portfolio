﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MiniGTimer : MonoBehaviour {

	public float mTimer;

	// Use this for initialization
	void Start () {
		mTimer = 10;
	}
	
	// Update is called once per frame
	void Update () {
		mTimer -= Time.deltaTime;

		if (mTimer <= 0){
			Application.LoadLevel ("Boom");
		}
	}
}
