﻿using UnityEngine;
using System.Collections;

public class mouseDrag : MonoBehaviour {

	float distance = 60;

	void OnMouseDrag () {
		Vector3 mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, distance);
		Vector3 objPosition = Camera.main.ScreenToWorldPoint(mousePosition);

		transform.position = objPosition;
	}

	void OnCollisionEnter (Collision col) {
		if (col.gameObject.tag == "Wall") {
			Application.LoadLevel ("Boom");
		}
		if (col.gameObject.tag == "End") {
			Application.LoadLevel ("Main");
		}
	}
}
