﻿using UnityEngine;
using System.Collections;

public class gameTimer : MonoBehaviour {

    public double boomTimer;
    public static bool gameRun = false;

    // Use this for initialization
    void Start () {
        boomTimer = Random.Range(40.0f, 70.0f);
        DontDestroyOnLoad(transform.gameObject);
	}
    public void gameStart ()
    {
        gameRun = true;
        Timer.timerDisplay = 5;
    }
	
	// Update is called once per frame
	void Update () {
        if (gameRun == true)
        {
            boomTimer -= Time.deltaTime;
        }
        if (boomTimer < 0)
        {
            Application.LoadLevel("Boom");
            Destroy (GameObject.FindWithTag("BoomTime"));
        }
    }
}
