﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ErrorText : MonoBehaviour {

	public Text errorPanelText;
	public AudioClip OK;
	public AudioClip Error;
	public int errorSound;
	public static int safeSound;

	private AudioSource source;

	// Use this for initialization
	void Start () {
		errorPanelText = GetComponent <Text> ();
		source = GetComponent <AudioSource> ();
		errorSound = 0;
		safeSound = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if (Timer.errorCheck == true){
			errorSound += 1;
			if (errorSound == 1){
			errorPanelText.color = Color.red;
			errorPanelText.text = "Err";
			source.PlayOneShot(Error,1);
		}

		if (Timer.errorCheck == false){
			errorSound = 0;
		}

		}
		if (Timer.safeText == true){
			safeSound += 1;
			if (safeSound == 1){
			errorPanelText.color = Color.green;
			errorPanelText.text = "O.K.";
			source.PlayOneShot(OK,1);
		}

		if (Timer.safeText == false){
			safeSound = 0;
		}

		}
		if (Timer.errorCheck == false && Timer.safeText == false){
			errorPanelText.color = Color.green;
			errorPanelText.text = "--";
		}

	}
}
