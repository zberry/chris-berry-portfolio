﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RocketCounter : MonoBehaviour {

	public float buttonPress;
	public float miniGameTimer;
	public Text pressTxt;

	void Start () {
		miniGameTimer = 10;
		pressTxt = GetComponent<Text>();
	}
	
	void Update () {
		pressTxt.text = "" + buttonPress;
		miniGameTimer -= Time.deltaTime;

		if (buttonPress >= 30){
			Application.LoadLevel("Main");
		}
		if (miniGameTimer <= 0){
			Application.LoadLevel("Boom");
		}
	}
	public void RocketButton() {
		buttonPress += 1;
	}
}
