﻿using UnityEngine;
using System.Collections;

public class ColorCube : MonoBehaviour {

	public int currentColor =0;
	public int numberColor =1;

	public GameObject blueTxt;
	public GameObject redTxt;
	public GameObject yellowTxt;
	public GameObject orangeTxt;

	void Start () {
		currentColor = Random.Range(1,5);
	}
	
	void Update () {
	
		if (currentColor == 1){
			//display blue text
			blueTxt.SetActive(true);
			redTxt.SetActive(false);
			yellowTxt.SetActive(false);
			orangeTxt.SetActive(false);
		}
		if (currentColor == 2){
			//display red text
			blueTxt.SetActive(false);
			redTxt.SetActive(true);
			yellowTxt.SetActive(false);
			orangeTxt.SetActive(false);
		}
		if (currentColor == 3){
			//display yellow text
			blueTxt.SetActive(false);
			redTxt.SetActive(false);
			yellowTxt.SetActive(true);
			orangeTxt.SetActive(false);
		}
		if (currentColor == 4){
			//display orange text
			blueTxt.SetActive(false);
			redTxt.SetActive(false);
			yellowTxt.SetActive(false);
			orangeTxt.SetActive(true);
		}
	
		if(Input.GetMouseButtonDown(0)) {
         Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
         RaycastHit hit;
         
         if(Physics.Raycast(ray, out hit, 100))
         {
         	if (hit.collider.tag == "BlueSide") {
         		Debug.Log("Blue");
         		if (currentColor == 1) {
				//color = Blue
				numberColor += 1;
				currentColor = Random.Range(1,5);
				}	
			else {
				Application.LoadLevel("Boom");
				}	
         	}
         	if (hit.collider.tag == "RedSide") {
         		Debug.Log("Red");	
         		if (currentColor == 2) {
				//color = Red
				numberColor += 1;
				currentColor = Random.Range(1,5);
				}
			else {
			Application.LoadLevel("Boom");
				}
         	}
         	if (hit.collider.tag == "YellowSide") {
         		Debug.Log("Yellow");
         		if (currentColor == 3) {
				//color = Yellow
				numberColor += 1;
				currentColor = Random.Range(1,5);
				}
			else {
			Application.LoadLevel("Boom");
				}	
     	   	}
         	if (hit.collider.tag == "OrangeSide") {
         		Debug.Log("Orange");	
         		if (currentColor == 4) {
				//color = Orange
				numberColor += 1;
				currentColor = Random.Range(1,5);
				}
			else {
			Application.LoadLevel("Boom");
				}
         	}
         }
       }
       		if (numberColor >= 4){
         		Application.LoadLevel("Main");
        }
	}
}
