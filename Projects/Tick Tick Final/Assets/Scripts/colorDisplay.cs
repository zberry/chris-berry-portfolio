﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class colorDisplay : MonoBehaviour {

	public Text colorText;
	
	// Use this for initialization
	void Start () {
		colorText = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
		colorText.text = "" + Timer.currentColor;
	}
}
