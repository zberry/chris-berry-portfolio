﻿using UnityEngine;
using System.Collections;

public class Timer : MonoBehaviour {

    public double timer = 5;
    public float gameNum;
    public float miniGameStart;
    public static int timerDisplay;
	public int color;
	public float miniCount;
	public float safeRefresh;
	public bool miniPick = false;
	public bool red = false;
	public bool blue = false;
	public bool green = false;
	public bool miniState = false;
	public bool miniCheck = false;
	public static bool errorCheck = false;
	public static bool safeText = false;
	public static string currentColor;

	// Use this for initialization
	void Start () {
		color = Random.Range(1, 4);
		miniGameStart = Random.Range(1,101);
		gameNum = 0;
		miniCount = 2;
		safeRefresh = 1;
		errorCheck = false;
	}

    public void redtimerUpdate() {
		if (color == 1) {
			timer = 5;
			miniCheck = true;
			safeRefresh = 1;
			errorCheck = false;
			if (miniGameStart <= 33){
				miniState = true;
				miniPick = true;
			}
			if (errorCheck == false){
				safeText = true;
				//play music for safe
			}
		}
		else
			Application.LoadLevel("Boom");
	
    }

	public void bluetimerUpdate() {
		if (color == 2) {
			timer = 5;
			miniCheck = true;
			safeRefresh = 1;
			errorCheck = false;
			if (miniGameStart <= 33){
				miniState = true;
				miniPick = true;
			}
			if (errorCheck == false){
				safeText = true;
				//play music for safe
			}
		}
		else
			Application.LoadLevel("Boom");
	}

	public void greentimerUpdate() {
		if (color == 3) {
			timer = 5;
			miniCheck = true;
			safeRefresh = 1;
			errorCheck = false;
			if (miniGameStart <= 33){
				miniState = true;
				miniPick = true;
			}
			if (errorCheck == false){
				safeText = true;
				//play music for safe
			}
		}
		else
			Application.LoadLevel("Boom");
	}  

	
	public void colorChange(){
		color = Random.Range(1, 4);
		miniGameStart = Random.Range(1,101);
	}


    // Update is called once per frame
    void Update() {
        if (gameTimer.gameRun == true) { 
        timer -= Time.deltaTime;
   	}
        if (timer > 4) {
            timerDisplay = 5;
    }
        if (timer > 3 && timer < 4) {
            timerDisplay = 4;}
        if (timer > 2 && timer < 3) {
            timerDisplay = 3;
    }
        if (timer > 1 && timer < 2) {
            timerDisplay = 2; 
    }
        if (timer > 0 && timer < 1) {
            timerDisplay = 1;
    }
        if (timer < 0) {
            timerDisplay = 0;
    }
        if (timer < 0) {
            Application.LoadLevel("Boom");
    }
		if (color == 1) {
			red = true;
			blue = false;
			green = false; 
	}
		if (color == 2) {
			red = false;
			blue = true;
			green = false; 
	}
		if (color == 3) {
			red = false;
			blue = false;
			green = true; 
	}
		if (color == 1) {
			currentColor = "PRESS: RED";
	}
		if (color == 2) {
			currentColor = "PRESS: BLUE";
	}
		if (color == 3) {
			currentColor = "PRESS: GREEN";
	}
		if (miniState == true) {
			gameTimer.gameRun = false;
	}
		if  (miniPick == true){
			gameNum = Random.Range(1, 6);
			miniPick = false;
	}
	//debug and used to get back from minigame smoothly [need to implement]
    	if (Input.GetKeyDown ("1")){
    		gameTimer.gameRun = true;
    		miniGameStart = 80;
    		miniState = false;
    		timerDisplay = 5;
    		color = Random.Range(1, 4);
    		gameNum = 0;
    }
   		if (gameNum == 1){
   			safeText = false;
   		 	errorCheck = true;
    		miniCount -= Time.deltaTime;
    		if (miniCount <= 0){
    		Application.LoadLevel("Game1");
    		}
    	}
    	if (gameNum == 2){
    		safeText = false;
    		errorCheck = true;
    		miniCount -= Time.deltaTime;
    		if (miniCount <= 0){
    		Application.LoadLevel("Game2");
    		}
    	}
    	if (gameNum == 3){
    		safeText = false;
    		errorCheck = true;
    		miniCount -= Time.deltaTime;
    		if (miniCount <= 0){
    		Application.LoadLevel("Game3");
    		}
    	}
    	if (gameNum == 4){
    		safeText = false;
    		errorCheck = true;
    		miniCount -= Time.deltaTime;
    		if (miniCount <= 0){
    		Application.LoadLevel("Game4");
    		}
    	}
    	if (gameNum == 5){
    		safeText = false;
    		errorCheck = true;
    		miniCount -= Time.deltaTime;
    		if (miniCount <= 0){
    		Application.LoadLevel("Game5");
    		}
    	}
    	if (errorCheck == true){
    		//play music for error
    	}
    	if (safeText == true){
    		safeRefresh -= Time.deltaTime;
    	}
    	if (safeRefresh <= 0){
    		safeText = false;
    		ErrorText.safeSound = 0;
    	}
  }
}

