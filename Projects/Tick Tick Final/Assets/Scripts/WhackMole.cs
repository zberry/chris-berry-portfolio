﻿using UnityEngine;
using System.Collections;

public class WhackMole : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

	if(Input.GetMouseButtonDown(0)) {
         Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
         RaycastHit hit;
         
        if(Physics.Raycast(ray, out hit, 100)) {
        	if (hit.collider.tag == "GoodMole") {
        		Application.LoadLevel("Main");
        	}
        	if (hit.collider.tag == "BadMole") {
        		Application.LoadLevel("Boom");
        	}
        }

     	}
	}
}
